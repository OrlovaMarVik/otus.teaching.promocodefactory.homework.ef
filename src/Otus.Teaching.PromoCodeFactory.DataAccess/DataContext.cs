﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        #region administration
        public DbSet<Role> Roles { get; set; }
        public DbSet<Employee> Employees { get; set; }
        #endregion

        #region management
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        #endregion

        public DbSet<CustomerPreference> CustomerPreferences{ get; set; }

        public DataContext()
        { }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Строковые поля должны иметь ограничения на MaxLength.
            foreach (var p in modelBuilder.Model
                .GetEntityTypes()
                .SelectMany(t => t.GetProperties())
                .Where(p => p.ClrType == typeof(string) && p.GetMaxLength() == null))
            {
                p.SetMaxLength(500);
            }

            modelBuilder.Entity<CustomerPreference>()
                .HasKey(p => new { p.CustomerId, p.PreferenceId });
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Customer)
                .WithMany(b => b.Preferences)
                .HasForeignKey(bc => bc.CustomerId);
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Preference)
                .WithMany()
                .HasForeignKey(bc => bc.PreferenceId);

            modelBuilder.Entity<Employee>()
                 .HasOne(p => p.Role)
                 .WithMany()
                 .HasForeignKey(p => p.RoleId);

            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.Preference)
                .WithMany()
                .HasForeignKey(p => p.PreferenceId);

            modelBuilder.Entity<Customer>()
                .HasMany(p => p.PromoCodes)
                .WithOne()
                .HasForeignKey(p => p.CustomerId);
        }
    }
}
