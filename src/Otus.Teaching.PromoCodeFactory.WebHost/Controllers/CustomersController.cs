﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {

        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, 
                                   IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        private Customer MapCreateOrEditCustomerRequest(Guid id, CreateOrEditCustomerRequest request)
        {
            return new Customer
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };
        }

        private CustomerResponse MapToResponse(Customer customer)
        {
            return new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = customer.Preferences?.Select(pref =>
                    new PreferenceResponse
                    {
                        Id = pref.Preference.Id,
                        Name = pref.Preference.Name,
                    }
                ).ToList(),
                PromoCodes = customer.PromoCodes?.Select(promo =>
                    new PromoCodeShortResponse
                    {
                        Id = promo.Id,
                        Code = promo.Code,
                        BeginDate = promo.BeginDate.ToString("dd.MM.yyyy"),
                        EndDate = promo.EndDate.ToString("dd.MM.yyyy"),
                        PartnerName = promo.PartnerName,
                        ServiceInfo = promo.ServiceInfo,
                    }
                ).ToList(),
            };
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersModelList = customers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                }).ToList();

            return customersModelList;
        }

        /// <summary>
        /// Получить данные клиента по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            return MapToResponse(customer);
        }

        /// <summary>
        /// Создать клиента и выдать его с его предпочтением и промокодом
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var id = Guid.NewGuid();
            var customerRequest = MapCreateOrEditCustomerRequest(id, request);

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            customerRequest.Preferences = preferences.Select(x => new CustomerPreference()
            {
                Customer = customerRequest,
                Preference = x
            }).ToList();

            await _customerRepository.AddAsync(customerRequest);

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            return MapToResponse(customer);
        }

        /// <summary>
        /// Изменить клиента 
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<CustomerResponse>> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Preferences.Clear();
            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                Customer = customer,
                Preference = x
            }).ToList();

            await _customerRepository.UpdateAsync(customer);
            
            var customerResult = await _customerRepository.GetByIdAsync(id);
            
            return MapToResponse(customerResult);
        }

        /// <summary>
        /// Удалить клиента 
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return Ok();
        }
    }
}