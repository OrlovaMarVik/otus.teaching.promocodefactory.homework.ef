﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {

        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Employee> _employeeRepository;

        public PromocodesController(IRepository<PromoCode> promoCodeRepository,
                                    IRepository<Preference> preferenceRepository,
                                    IRepository<Customer> customerRepository,
                                    IRepository<Employee> employeeRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
            _employeeRepository = employeeRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();
            var promoCodeList = promoCodes.Select(promo =>
                new PromoCodeShortResponse
                {
                    Id = promo.Id,
                    Code = promo.Code,
                    BeginDate = promo.BeginDate.ToString("dd.MM.yyyy"),
                    EndDate = promo.EndDate.ToString("dd.MM.yyyy"),
                    PartnerName = promo.PartnerName,
                    ServiceInfo = promo.ServiceInfo,
                 }).ToList();

            return promoCodeList;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            //должен сохранять новый промокод в базе данных и находить клиентов с переданным предпочтением и добавлять им данный промокод

            var preferences = await _preferenceRepository.GetAllAsync();
            var preference = preferences.FirstOrDefault(p => p.Name == request.Preference);

            if (preference == null)
                throw new Exception("Невозможно создать промокод с несуществующим предпочтением");

            var customers = await _customerRepository.GetAllAsync();
            var employees = await _employeeRepository.GetAllAsync();

            if (customers.Any())
            {
                foreach (Customer customer in customers.Where(p => p.Preferences.Any(pr => pr.PreferenceId == preference.Id)))
                {
                    var newPromoCode = new PromoCode()
                    {
                        Id = Guid.NewGuid(),
                        Code = request.PromoCode,
                        ServiceInfo = request.ServiceInfo,
                        PartnerName = request.PartnerName,
                        BeginDate = DateTime.Now,
                        EndDate = DateTime.Now.Date.AddDays(7),
                        PreferenceId = preference.Id,
                        CustomerId = customer.Id,
                        EmployeeId = employees.First().Id, 

                    };

                    await _promoCodeRepository.AddAsync(newPromoCode);
                }
            }

            return NoContent();
        }
    }
}