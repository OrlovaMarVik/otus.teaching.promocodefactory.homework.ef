﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;

        public PreferencesController(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить данные по всем предпочтениям
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PreferenceResponse>> GetPreferencesAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();

            var preferencesModelList = preferences.Select(x =>
                new PreferenceResponse()
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();

            return preferencesModelList;
        }

        /// <summary>
        /// Получить данные предпочтения по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PreferenceResponse>> GetPreferenceAsync(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);

            if (preference == null)
                return NotFound();

            var preferenceModel = new PreferenceResponse()
            {
                Id = preference.Id,
                Name = preference.Name,
            };

            return preferenceModel;
        }

        /// <summary>
        /// Создать предпочтение
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<PreferenceResponse>> CreatePreferenceAsync(string name)
        {
            var id = Guid.NewGuid();
            await _preferenceRepository.AddAsync(new Preference { Id = id, Name = name});

            var preference = await _preferenceRepository.GetByIdAsync(id);

            if (preference == null)
                return NotFound();

            var preferenceModel = new PreferenceResponse()
            {
                Id = preference.Id,
                Name = preference.Name,
            };

            return preferenceModel;
        }

        /// <summary>
        /// Изменить предпочтение 
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<PreferenceResponse>> EditPreferencesAsync(Guid id, string name)
        {
            await _preferenceRepository.UpdateAsync(new Preference { Id = id, Name = name });

            var preference = await _preferenceRepository.GetByIdAsync(id);

            if (preference == null)
                return NotFound();

            var preferenceModel = new PreferenceResponse()
            {
                Id = preference.Id,
                Name = preference.Name,
            };

            return preferenceModel;
        }

        /// <summary>
        /// Удалить предпочтение 
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeletePrefernce(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);

            if (preference == null)
                return NotFound();

            await _preferenceRepository.DeleteAsync(preference);

            return Ok();
        }
    }
}
